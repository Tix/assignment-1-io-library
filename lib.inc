section .text
 %define EXIT 60
 %define NULL_TERMINATOR 0
 %define MIN '0'
 %define MAX '9'
 %define SPACE 0x20
 %define TAB 0x9
 %define NEW_STRING 0xA
 %define STDOUT 1
 %define SYS_CALL 1

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT 
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; обнуляем итератор 
    .loop:
        cmp byte[rdi+rax], NULL_TERMINATOR  ;если символ в строке 0, то завершаем метод
        je .terminate
        inc rax ; инкрементируем итератор для перехода на следующий символ в строке
        jmp .loop ; повтораяем вышеперечисленные операции до ожидания 0
    .terminate:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi ;
    call string_length
    pop rdi
    mov rdx, rax ; length.str -> rdx 
    mov rsi, rdi ; передаем адрес строки
    mov rdi, STDOUT ; передаем дескриптор
    mov rax, SYS_CALL ; передаем номер syscoll'а 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    ; syscoll принимает адресс элемента, а не значение самого элемента
    push rdi
    mov rsi,rsp ; поэтому
    pop rdi
    mov rdi, STDOUT
    mov rdx,1
    mov rax,SYS_CALL
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA) 
print_newline:
    xor rax, rax
    mov rdi , '\n' 
    jmp print_char ; мы положили в регистр нужный символ и вызвали функцию по выводу символа


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r10, 10; основание СС
    mov r8, rsp ; сохраняем указатель стека
    ; будем записывать наши цифры в стек чтобы вывести все функцией  print_string
    push 0 ; чтобы строка была 0 терминированной 
    mov rax, rdi ; записываем в аккумулятор исходное число 
    .loop:
        xor rdx, rdx ; зануляем дата регистр 
        div r10 ; остаток уходит в дата регистр 
        add rdx, '0' ; кодировка ASCII устроена таким образом что цифра в 10 СС начинаются с 30 
        ; и заканчиваются 39ю, естественно в 16ой СС, поэтому мы прибавляем 0 к каждой цифре 
        ; код 0 в 16СС = 30, следовательно итоговый вид символа будет представлен именно в ASCII, вроде все
        dec rsp ; сдвигаем указатель стека
        mov byte[rsp], dl ; понятно что символ кодируется 1 байтом, поэтому берем младшие 8 бит 
        test rax, rax
        je .terminate
        jmp .loop
    
    .terminate:
        mov rdi, rsp 
        push r8 ;сохраняем указатель стека
        call print_string
        pop r8
        mov rsp, r8 ; приводим указатель стека к исходному значению 

        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0 
    jl .neg_case  ; если рез отрицательный, то просто выводим строку 
    jmp print_uint ; рез должен быть отрицательным потому что разница берется между вторым и первым аргументом 
    .neg_case:
        push rdi 
        mov rdi, '-'
        call print_char
        pop rdi 
        neg rdi 
        jmp print_uint ; сначала выводим "-", затем чисто по модулю 
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    cmp rdi, rsi ; если указатель один и тот же, но строки очевидно равны 
    je .true
   
    .loop:
    mov r8b, [rdi] ; для цифр достаточно байта 
    mov r9b,[rsi]
    cmp r8b, r9b 
    jne .false 
    test r8b, r8b ; проверяем окончание строк
    je .true
    inc rdi
    inc rsi 
    jmp .loop 
    .true:
        mov rax, 1
        ret 
    .false: 
        xor rax, 0
        ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    xor rax, rax ; хоп, уже правильный дескриптор 
    push 0 
    xor rdi, rdi 
    mov rdx, 1
    mov rsi, rsp ; устнавливаем адрес начала записи 
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;rdi - адрес начала буфера, rsi - размер буфера 
read_word:
    
    .passer:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp rax, SPACE
        je .passer
        cmp rax, TAB
        je .passer 
        cmp rax, NEW_STRING
        je .passer

    xor rcx, rcx

    .main_loop:
        test rax, rax;
        je .set_null
        cmp rax, SPACE
        jz .set_null
        cmp rax, TAB
        je .set_null
        cmp rax, NEW_STRING
        je .set_null

        cmp rcx, rsi ;проверка на переполнение буфера 
        jg .break 

        mov [rdi + rcx], rax
        inc rcx 

        push rcx
        push rdi
	push rsi
        call read_char
	pop rsi
        pop rdi
        pop rcx
	jmp .main_loop
    .set_null:
        mov byte[rdi+rcx],0
        mov rdx, rcx
        mov rax, rdi
        ret
    .break:
        xor rax, rax
        xor rdx, rdx
        ret 

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx ; итерировать будем по этому регистру, для упрощения вывода количества символов в строке
  
    xor rax, rax
    xor r8, r8
    .loop: 
        mov r8b, byte[rdi + rdx]

        cmp r8b, MIN
        jl .terminate

        cmp r8b, MAX
        jg .terminate ; проверяем относятся ли прочитанные символы цифрам

        imul rax, 10
	sub r8b, '0' ; переводим из ASCII
        add rax, r8 ; переведя разряд прибавляем распарсенное число 
        inc rdx ; увеличиваем счетчик
        jmp .loop
    .terminate:
        ret 


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi
    cmp byte[rcx], '-'
    je .print_neg
    jmp .print_pos

    .print_neg:
    ; нужно пропустить минус
    inc rdi
    call parse_uint
    neg rax
    inc rdx ; учет минуса 
    ret
    .print_pos:
    
    call parse_uint
    
    
    ret 






    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
   xor rax, rax
    ; проверим длину строки готовой функцией, для этого засушим в стек все полезный регистры 
    push rdx
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    pop rdx 
 
    cmp rdx, rax 
    jl .break
    .loop:
        cmp rcx, rax
        jg .terminate
        mov r10,[rdi+rcx] 
        mov [rsi+rcx], r10
        inc rcx
        jmp .loop
    
    .break:
        xor rax, rax ;возвращаем 0
        ret
    .terminate:
        ret
